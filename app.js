var express = require("express")
var app = express()
app.set('trust proxy', true)
app.get("/",(req,res)=>{
    res.send("Hello world!")
})

app.get("/ipaddress",(req,res)=>{
    console.log(req.ip)
    res.send(req.ip)
})

app.get("/ipsocket",(req,res)=>{
    console.log(req.ip)
    res.send(req.socket.remoteAddress)
})

app.get("/ipsocketignoreproxy",(req,res)=>
{   console.log(req.ip)
    res.send(req.headers['x-forwarded-for'])
})

app.get("/trythistoo",(req,res)=>
{
    var ip = req.headers['x-forwarded-for'] ||
     req.socket.remoteAddress ||
     null;
     console.log(req.ip)
     res.send(ip)
})

app.listen(3000,function (){
    
    console.log("Example app listening on port 3000!")
})
